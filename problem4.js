const inventory = require("./inventory")

let problem4 = (inventory, key_name) => {
    if (!Array.isArray(inventory) || inventory.length <= 0) {
        return "first argument must be an array";
      }
      if (!inventory.every((each) => typeof each === "object")) {
        return "array must be an arrray of object";
      }
      let found_key = inventory.every((each) => {
        return Object.keys(each).includes(key_name);
      });
      if (!found_key) {
        return "key not found in object"
      }
      if (typeof key_name === "string") {
        if (key_name.length <= 0) {
          return "Invalid String";
        }
      }
      if (typeof key_name === "number") {
        if (key_name < 0) {
          return "must provide a positive integer";
        }
      }
    let ans = inventory.map(element => {
        return element[key_name]
    })
    return ans
}
//console.log(problem4(inventory, "car_year"));
module.exports = problem4