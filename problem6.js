const inventory = require("./inventory");

let problem6 = (inventory, key_name, interested_cars) => {
  if (!Array.isArray(inventory) || inventory.length <= 0) {
    return "first argument must be an array";
  }
  let found_key = inventory.every((each) => {
    return Object.keys(each).includes(key_name);
  });
  if (!found_key) {
    return "key is not present in object";
  }
  let ans = [];
  if (!Array.isArray(interested_cars) || interested_cars.length <= 0) {
    return "please provide an array of interested cars";
  }
  ans = inventory.filter((element) => {
    return interested_cars.includes(element[key_name]);
  });
  return ans;
};

//console.log(problem6(inventory, "car_make", ["BMW", "Audi"]));
module.exports = problem6;
