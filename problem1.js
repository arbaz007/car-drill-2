const inventory = require("./inventory");

let problem1 = (inventory, id) => {
  if (!Array.isArray(inventory) || inventory.length <= 0) {
    return "first arguent must be an array";
  }
  if (!inventory.every((each) => typeof each === "object")) {
    return "array must be an arrray of object";
  }
  if (isNaN(id) || id < 0) {
    return "id must be an positive integer";
  }
  let car_33 = inventory.reduce((acc, element) => {
    if (element.id == id) {
      acc = element;
    }
    return acc;
  }, {});
  return car_33;
};
module.exports = problem1;
