const inventory = require("../inventory")
const problem6 = require("../problem6")

let result = problem6(inventory, "car_make", ["Audi", "BMW"])
console.log(JSON.stringify(result));