let problem3 = (inventory, key) => {

    if (!Array.isArray(inventory) || inventory.length <= 0)  {
        return "first argument must be an array"
    }
    if (!inventory.every(each => typeof each === "object" || typeof each === null)) {
        return "array must be an array of object" 
    }
    let found_key = inventory.every(each => {
        return Object.keys(each).includes(key)
      })
    if (!found_key) {
        return "key not found in object"
    }
    inventory.sort ((obj1, obj2) => {
        if (obj1[key] < obj2[key]) {
            return -1 ;
        }else {
            return 0;
        }
    })
    let ans = inventory.map (each => {
        return each[key]
    })
    return ans
}

module.exports = problem3