const inventory = require("./inventory");
const problem4 = require("./problem4");
let problem5 = (inventory, key_name, key_value) => {
  if (!Array.isArray(inventory) || inventory.length <= 0) {
    return "first argument must be an array";
  }
  if (!inventory.every((each) => typeof each === "object")) {
    return "array must be an array of object";
  }
  let found_key = inventory.every((each) => {
    return Object.keys(each).includes(key_name);
  });
  if (!found_key) {
    return "key not found in object"
  }
  if (!typeof key_value === "number") {
    return "third argument must be an integer";
  }
  if (key_value <= 0) {
    return "must provide positive integer";
  }
  let years = problem4(inventory, "car_year");
  if (years == null || years.length == 0) {
    return "No years found"
  }
  let ans = [];
  years.forEach((each, index) => {
    if (inventory[index][key_name] < key_value) {
      ans.push(inventory[index]);
    }
  });
  return ans;
};

// problem5(inventory, "car_year", 2000)
module.exports = problem5;
