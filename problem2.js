const inventory = require('./inventory');

let problem2 = (inventory) => {
    if (!Array.isArray(inventory) || inventory.length <= 0) {
        return "first arguent must be an array"
    }
    if (!inventory.every(each => typeof each === "object" || typeof each === null)) {
        return "array must be an array of object" 
    }
    let last_car = inventory.filter((each)=> {
        return each.id == inventory.length
    })
    return last_car[0]
}

module.exports = problem2
